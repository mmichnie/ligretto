package LigrettoGUI;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MojPanel extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6450928759020487807L;
	private JTextField surnameField;
	private JTextField nameField;
	
	public MojPanel() {
		JLabel nameLabel = new JLabel("ImiÄ™:");
		nameField = new JTextField();
		JLabel surnameLabel = new JLabel("Nazwisko:");
		surnameField = new JTextField();
		JButton submitButton = new JButton("Zapisz");
		
		//pierwszy wiersz
		this.add(nameLabel);
		this.add(nameField);
		//drugi wiersz
		this.add(surnameLabel);
		this.add(surnameField);
		//trzeci wiersz
		this.add(new JLabel());
		this.add(submitButton);
		
		submitButton.addActionListener(this);
		
		this.setLayout(new GridLayout(3, 2, 5, 5));

	}

//	@Override
	public void actionPerformed(ActionEvent e) {
		String nameValue = nameField.getText();
		String surnameValue = surnameField.getText();
		System.out.println(nameValue + " " + surnameValue);
		nameField.setText("");
		surnameField.setText("");
		
		System.out.println("Action command: " + e.getActionCommand());
		System.out.println("ID: " + e.getID());
		System.out.println("Modifiers: " + e.getModifiers());
		System.out.println("Source: " + e.getSource());
		System.out.println("Class: " + e.getClass());
		System.out.println("When: " + new Date(e.getWhen()));
		
		if (e.getSource() instanceof JButton) {
			JButton button = (JButton) e.getSource();
			button.setText("Zapisano");
		}

	}

}
