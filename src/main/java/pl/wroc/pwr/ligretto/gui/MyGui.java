package pl.wroc.pwr.ligretto.gui;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

import pl.wroc.pwr.ligretto.model.Player;




public class MyGui {

	public MyGui(){
	}
	
	public void stworzGui(Player player1, Player player2, Player player3, Player player4) {
		JFrame frame = new JFrame();
		frame.setSize(400,400);
		frame.setTitle("Ligretto");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		PanelPlayer panel1 = new PanelPlayer(player1);
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));
		PanelPlayer panel2 = new PanelPlayer(player2);
		panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));
		PanelPlayer panel3 = new PanelPlayer(player3);
		panel3.setLayout(new BoxLayout(panel3, BoxLayout.Y_AXIS));
		PanelPlayer panel4 = new PanelPlayer(player4);
		panel4.setLayout(new BoxLayout(panel4, BoxLayout.Y_AXIS));
		
		frame.getContentPane().add(BorderLayout.SOUTH, panel1);
		frame.getContentPane().add(BorderLayout.NORTH, panel2);
		frame.getContentPane().add(BorderLayout.EAST, panel3);
		frame.getContentPane().add(BorderLayout.WEST, panel4);
		
		Table mainTable = new Table();
		frame.getContentPane().add(BorderLayout.CENTER, mainTable);
		
		frame.setVisible(true);
		
	}
}
