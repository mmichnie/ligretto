package pl.wroc.pwr.ligretto.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.Date;

public class Table extends JPanel implements ActionListener {

	private static final long serialVersionUID = 6450928759020487807L;
	private JTextField surnameField;
	private JTextField nameField;
	
	public Table() {
			narysujStol();
	}

	private void narysujStol() {
		this.setLayout(new GridLayout(4, 4));
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				JButton kupka = new JButton();
				kupka.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
				this.add(kupka);
			}
		}		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		String nameValue = nameField.getText();
		String surnameValue = surnameField.getText();
		System.out.println(nameValue + " " + surnameValue);
		nameField.setText("");
		surnameField.setText("");
		
		System.out.println("Action command: " + e.getActionCommand());
		System.out.println("ID: " + e.getID());
		System.out.println("Modifiers: " + e.getModifiers());
		System.out.println("Source: " + e.getSource());
		System.out.println("Class: " + e.getClass());
		System.out.println("When: " + new Date(e.getWhen()));
		
		if (e.getSource() instanceof JButton) {
			JButton button = (JButton) e.getSource();
			button.setText("Zapisano");
		}

	}

}

