package pl.wroc.pwr.ligretto.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

import pl.wroc.pwr.ligretto.model.Card;
import pl.wroc.pwr.ligretto.model.Player;

public class PanelPlayer extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = -2130920653556661891L;
	Player player;
	int i=0;
	
	public PanelPlayer(Player player) {
		this.player=player;
		for (int i = 0; i < 3; i++) {
			Card karta = player.row.get(i);
			JButton rzad = new JButton(karta.string_card_nr());
			rzad.setBackground(karta.getKolor());
			rzad.setForeground(Color.WHITE);
			rzad.setSize(100, 100);
			rzad.addActionListener(this);
			this.add(rzad);
			rzad.setEnabled(false);
		}
		this.add(Box.createRigidArea(new Dimension(10, 10)));
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		Card karta = player.pile.get(i);
		JButton stos = new JButton(karta.string_card_nr());
		stos.setBackground(karta.getKolor());
		stos.setForeground(Color.WHITE);
		stos.setSize(100, 100);
		stos.addActionListener(this);
		this.add(stos);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(i==player.pile.size())
			i=0;
		JButton button = (JButton) e.getSource();
		if (!player.pile.isEmpty()) {
			Card karta = player.pile.get(i); 
			button.setBackground(karta.getKolor());
			button.setText(karta.string_card_nr());
			i++;
		} else {
			button.setBackground(Color.GRAY);
			button.setText("");
			button.setEnabled(false);
		}
		button.repaint();
	}
}
