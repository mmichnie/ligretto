package pl.wroc.pwr.ligretto.game;

import pl.wroc.pwr.ligretto.gui.MyGui;
import pl.wroc.pwr.ligretto.model.Cardset;
import pl.wroc.pwr.ligretto.model.Computer;
import pl.wroc.pwr.ligretto.model.Human;
import pl.wroc.pwr.ligretto.model.Player;
import pl.wroc.pwr.ligretto.model.Sign;

public class Game {

	private Player[] players;
	
	public Game() {
		players = new Player[4];
	}
	
	public static void main(String[] args) {
		Game game = new Game();

		Cardset cards1=new Cardset(Sign.Cube);
		Cardset cards2=new Cardset(Sign.Crystal);
		Cardset cards3=new Cardset(Sign.Pyramid);
		Cardset cards4=new Cardset(Sign.Sphere);
		
		cards1.shuffle();
		cards2.shuffle();
		cards3.shuffle();
		cards4.shuffle();
		
		Player player1 = new Human(cards1);
		Player player2 = new Computer(cards2);
		Player player3 = new Computer(cards3);
		Player player4 = new Computer(cards4);
		
		game.addPlayer(1,player1);
		game.addPlayer(2,player2);
		game.addPlayer(3,player3);
		game.addPlayer(4,player4);
		
		game.play(player1,player2,player3,player4);

	}
	


	public Player[] getPlayers() {
		return players;
	}

	public void play(Player player1, Player player2, Player player3, Player player4) {
		System.out.println("Welcome to Ligretto!");
		MyGui gui=new MyGui();
		gui.stworzGui(player1, player2, player3, player4);
		
	}

	public void addPlayer(int which, Player player) {
		players[which-1] = player;
		
	}
	
}
