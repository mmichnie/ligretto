package pl.wroc.pwr.ligretto.model;

import java.awt.Color;
public class Card implements Comparable<Card> {


	private Color color;
	private int nr;
	private Sign sign;
	
	public Card(Color color, int nr, Sign sign)
	{
		this.color=color;
		this.nr=nr;
		this.sign=sign;
	}
	
	public int getnumber()
	{return this.nr;}
	
	public String string_card_nr()
	{return ""+this.nr;}
	
	public Color getKolor() {
		return this.color;
	}
	
	@Override
	public int compareTo(Card o) {
		
		if(nr==o.nr)
			return 0;
		else if(nr < o.nr)
			return -1;
		else
			return 1;
	}


}
