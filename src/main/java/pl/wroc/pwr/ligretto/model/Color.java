package pl.wroc.pwr.ligretto.model;

public enum Color {
	Yellow, Red, Green, Blue;
}
