package pl.wroc.pwr.ligretto.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class Player {

	public List<Card> hand;
	public List<Card> row;
	public List<Card> pile;
	
	public abstract Card[] getCards();
	
	public Player(Cardset cards)
	{
		pile= new ArrayList<Card>();
		row= new LinkedList<Card>();
		hand= new LinkedList<Card>();
		
		for(int i=0;i<40;i++)
		{
			if(i<10)
				pile.add(cards.getCards().get(i));
			else if(9<i && i<13)
				row.add(cards.getCards().get(i));
			else
				hand.add(cards.getCards().get(i));
		}
	}

}
