package pl.wroc.pwr.ligretto.model;

import java.awt.Color;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Cardset {
	
	public List<Card> cards;
	
	public Cardset(Sign sign)
	{
		cards=new LinkedList<Card>();
		
		Color col=Color.YELLOW;
		for(int i=1 ; i<=10 ; i++){
			Card c=new Card(col,i,sign);
			cards.add(c);}
		
		col=Color.BLUE;
		for(int i=1 ; i<=10 ; i++){
			Card c=new Card(col,i,sign);
			cards.add(c);}
		
		col=Color.GREEN;
		for(int i=1 ; i<=10 ; i++){
			Card c=new Card(col,i,sign);
			cards.add(c);}
		
		col=Color.RED;
		for(int i=1 ; i<=10 ; i++){
			Card c=new Card(col,i,sign);
			cards.add(c);}
	}
	public boolean shuffle()
	{
		try{
		Collections.shuffle(cards);
		return true;}
		
		catch(Exception exc){
			System.out.println("nie udalo sie potasowac kart");
			return false;}
		
	}
	
	public List<Card> getCards() {
		return cards;
	}

}
